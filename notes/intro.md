# Introductionary lecture

## Scientific computing and numerical simulation

- **response time** - compute a problem in $\frac{1}{p}$ time
- **problem size** - compute a $p$-times bigger problem
- **throughput** - compute $p$ problems at once

> Numerical simulations on highly parallel multicore ($10^3$) systems. $O(n * log(n))$ algorithms are bare minimum and $O(n^2)$ complexity is not acceptable
##HPC in literature - Past and Present trends

The seven dwarfs in HPC:

1. Dense linear algebra
2. Sparse linear algebra
3. *skipped* spectral methods
4. N-body methods
5. structured grids
6. unstructured grids
7. *skipped* Monte Carlo (stochastic schemes)

Challenges

1. Performance - exponential growth in performance
2. Programming - new (parallel) programming models
3. Prediction - reproducibility of experiments.

## Organization
Tutorials - GPU computing using CUDA. Tutorials are bi-weekly.

In general, the schedule is *messed up* (some lectures are skipped and swapped with tutorials) and should be checked for every week.

Presumably exam on 2018.03.01
