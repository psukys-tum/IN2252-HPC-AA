# Dense linear algebra

> Recap of previous tutorial, solution for HW 1

Roofline model: at some point, algorithm is just capped by hardware itself.

Roofline for mmult with `TILE_SIZE = 8`:

- $2 * 4 * \frac{n}{TILE\_SIZE}$ global reads (4 bytes for float)
- $2 * TILE_SIZE$ Flops
- 2 Flops/B

## Warp serialize

NVidia GPU does an optimization if aligned (one after another) memory can be loaded in bulk instead of loading one-by-one.

- be in order
- no double access
- Matrix layout in CUDA is row-wise

## Coalescization

1 dimensional block (say 128):

- each block has 128 threads (0..127)
- threads separated into warps of 32 (0..31, 32..63, ...) subblocks

2 dimensional block:
- 16*16 matrix
- warps up to 32 (at this point 16, as it's coalesced line)
- row major

## Overlapping

Include prefetching of blocks to reduce *idle* time for memory transfer.
