# CUDA introduction

> HPC auth in  Other/TUM/HPC_LAB

Host - CPU, hosting the application
Device - GPU with device memory

Hardware characteristics:

- massively parallel
- cheap threads
- massive parallelism hides memory latency (data parallelism is more important)


## Example: Matrix-Matrix Multiplication (mmult)

`cudaMalloc(void **ppd, int size)`
Instead of receiving a pointer, one needs to be passed through `ppd`. `size` in bytes

`cudaFree(void *pd)`

`cudaMemcpy(pd, p, size, deviceToHost/hostToDevice)` - dest, src, size, constant of stuff done


## Compiling on cluster

1. `module load gcc/4.8`
2. `module load cuda/6.5`
3. `nvcc ..`
4. `salloc --ntasks=1 --partition=nvd`
5. `srun ./application`
6. `exit` - free up `salloc`

## Homework

Implement tile matrix-matrix multiplication
