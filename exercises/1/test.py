"""Verifies matrix multiplication result for exercise 1.2 - Tiled matrix mult."""
import subprocess
import re


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    """Float lax comparison https://stackoverflow.com/a/33024979/552214"""
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def verify_matrix(c):
    """Verifies matrix that it's diagonal and values are (n+1)/2."""
    val = (len(c) + 1) / 2.0

    for row_idx, row in enumerate(c):
        for col_idx, cell in enumerate(row):
            if row_idx == col_idx:
                if not isclose(cell, val, abs_tol=0.001):
                    print('Expected [{2}, {3}] value {0}, got {1}'.format(
                        val, cell, row_idx, col_idx))
                    return False
            else:
                if int(cell) != 0:
                    print('Expected [{2}, {3}] value {0}, got {1}'.format(
                        0, int(cell), row_idx, col_idx))
                    return False

    return True


def extract_matrix(output):
    """Extracts matrix from given output."""
    start = output.find('Matrix C')
    c_start = output.find('\n', start) + 1
    c_end = output.find('Elapsed') - 2

    return [[float(cell) for cell in row.strip().split(' ')] for row in output[c_start:c_end].split('\n')]


def verify(exec_name):
    """Does verification and returns boolean and performance, timing for run."""
    command = ['srun', exec_name, '257', '1']
    p = subprocess.Popen(command, stdout=subprocess.PIPE)

    output = p.communicate()[0]

    c = extract_matrix(output)
    valid = verify_matrix(c)

    match = re.search('Elapsed time\s+:\s+(\d+\.\d+) s', output)
    runtime = match.group(1)

    match = re.search('Performance\s+:\s+(\d+)\s+MFlop/s', output)
    performance = match.group(1)

    return {'valid': valid, 'perf': performance, 'runtime': runtime}


if __name__ == '__main__':
    execs = ['cpu', 'global', 'tiled']
    for ex in execs:
        print(ex)
        print(verify(ex))
