#include "cuda_mmult_kernels.h"
#include <stdio.h>

/*
 * matrix multiplication C += A*B
 *  -> CUDA kernel
 *     (implementation adopted from Kirk&Hwu:
 *      "Programming Massively Parallel Processors, chapter 4)
 *  -> Features: none (basic tiled version, using only global memory)
 */
__global__ void matrixMultKernel_global(float* Ad, float* Bd, float* Cd, int n)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int k = blockIdx.y * blockDim.y + threadIdx.y;

    float Celem = 0;

    for(int j=0; j<n; j++) {
        float Aelem = Ad[i*n+j];
        float Belem = Bd[j*n+k];
        Celem += Aelem*Belem;
    }

    Cd[i*n+k] += Celem;
}

/** Transforms 2D into 1D value
 * @param row - x value
 * @param col - y value
 * @param n - dimension size
 *
 * 1    2   3   4
 * 5    6   7   8
 * 9    10  11  12
 * 13   14  15  16
 */
__device__ int two_to_one_dim(int row, int col, int n) {
    return row * n + col;
}

/*
 * matrix multiplication C += A*B
 *  -> CUDA kernel
 *     (implementation adopted from Kirk&Hwu:
 *      "Programming Massively Parallel Processors, chapter 5)
 *  -> Features:
 *     - tiled matrix multiplication with use of shared memory
 */
__global__ void matrixMultKernel_tiled(float* A, float* B, float* C, int n)
{
    // local block's cache
    __shared__ float A_cached[TILE_SIZE][TILE_SIZE];
    __shared__ float B_cached[TILE_SIZE][TILE_SIZE];

    // Cell value that thread is responsible for
    float c = 0.0;

    // block indices in grid (global?)
    int bx = blockIdx.x;
    int by = blockIdx.y;

    // thread indices in block's scope
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    // thread indices in global scope
    int tgx = bx * TILE_SIZE + tx;
    int tgy = by * TILE_SIZE + ty;

    // Do block sized copies and mm
    for (int step = 0; step < (n - 1)/ TILE_SIZE + 1; step++) {
        // same row, different columns
        A_cached[tx][ty] = (ty + step * TILE_SIZE >= n || tgx >= n) ? 0.0f : A[two_to_one_dim(tgx, ty + step * TILE_SIZE, n)];

        // same col, different rows
        B_cached[tx][ty] = (tx + step * TILE_SIZE >= n || tgy >= n) ? 0.0f : B[two_to_one_dim(tx + step * TILE_SIZE, tgy, n)];

        // wait until memory is prepared
        __syncthreads();
        for (int j = 0; j < TILE_SIZE; j++) {
            c += A_cached[tx][j] * B_cached[j][ty];
        }

        // wait so that local shared cache doesn't get overwritten
        __syncthreads();
    }
    if (tgx < n && tgy < n) {
        C[two_to_one_dim(tgx, tgy, n)] = c;
    }
}
